﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCan : MonoBehaviour
{
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<InteractableObject>())
        {
            GameObject objectToDestory = collision.gameObject;
            Destroy(objectToDestory);
            Debug.Log("Destroy!!!!");
        }
    }
}
