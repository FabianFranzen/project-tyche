﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    Scene _currentScene;
    public void Awake()
    {
        _currentScene = SceneManager.GetActiveScene();
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("Test Scene");
        SceneManager.UnloadSceneAsync(_currentScene);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LoadMainMenu()
    {
        SceneManager.UnloadSceneAsync(_currentScene);
        SceneManager.LoadScene(0);
    }
}