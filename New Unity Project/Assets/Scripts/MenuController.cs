﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public Animator _menuAnimator;

    public void MenuToggle()
    {
        Debug.Log("Button Pressed");
        bool previousValue = _menuAnimator.GetBool("MenuIsOpen");
        _menuAnimator.SetBool("MenuIsOpen", !previousValue);
    }
}
