﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseSpawner : MonoBehaviour
{
    public int _roundsLeft = 10;

    [HideInInspector]
    public MagazineSlot _magazineSlot;

    private HandBehaviour _holdingHand;



    public void AssignHolder(HandBehaviour handBehaviour)
    {
        _holdingHand = handBehaviour;
        Debug.Log(_holdingHand);
    }

    private void OnTriggerEnter(Collider collider)
    {
        MagazineSlot magazineSlot = collider.GetComponent<MagazineSlot>();
        if (magazineSlot != null && _holdingHand != null)
        {
            _holdingHand.DroppedObject();
            _holdingHand = null;
            magazineSlot.AttachMagazine(this);

            GetComponent<Rigidbody>().isKinematic = true;

            Transform attachPoint = magazineSlot._attachPoint;


            transform.position = attachPoint.position;
            transform.rotation = attachPoint.rotation;
            transform.parent = attachPoint;

        }
    }
    
}

   