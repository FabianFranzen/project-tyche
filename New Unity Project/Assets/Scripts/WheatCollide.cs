﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheatCollide : MonoBehaviour
{

    
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<HouseResource>())
        {
            ScoreManager.Instance.scoreIncrease();

            Destroy(gameObject);
        }
    }
}