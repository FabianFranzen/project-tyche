﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public int _playerScore = 0;
    private Targets _targetReference;

    public void AddPoints()
    {
        _playerScore++;
        Debug.Log("Score: " + _playerScore);
    }
}
