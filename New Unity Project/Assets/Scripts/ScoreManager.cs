﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : Singleton<ScoreManager>
{
    public Text _canvasText;

    int happiness = 0;
    public string happinessString = "Happiness : 0";

    
    public void scoreIncrease()
    {
        happiness++;
        happinessString = "Happiness : " + happiness;
        _canvasText.text = happinessString;
        Debug.Log("Happiness increased to " + happiness);
    }
}
