﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazineSlot : MonoBehaviour
{
    public HouseSpawner _currentMagazine;

    public Transform _attachPoint;
    
    public void AttachMagazine(HouseSpawner magazine)
    {
        if (_currentMagazine == null)
        {
            _currentMagazine = magazine;

            _currentMagazine._magazineSlot = this;

            Debug.Log("Attached Magazine");
        }

        else
        {
            Debug.LogError("Error attaching magazine");
        }
    }

    public void DetachMagazine()
    {
        if (_currentMagazine != null)
        {
            
            _currentMagazine._magazineSlot = null;

            _currentMagazine = null;



            Debug.Log("Detached Magazine");
        }

        else
        {
            Debug.LogError("Error detaching magazine");
        }
    }
}
