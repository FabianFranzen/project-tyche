﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //save the bullet position at the end of update
    private Vector3 _lastBulletLocation;
    private bool _startedMoving;
    
    //Raycast from old position to new position, see if we hit anything
    private Rigidbody _hitobject;
    //Stop the bullet, and add bullethole

    private void Awake()
    {

    }

    private void Update()
    {
        RaycastHit raycastHit;
        Vector3 bulletDirection = transform.position -_lastBulletLocation;
        if(_startedMoving && Physics.Raycast(_lastBulletLocation, bulletDirection, out raycastHit, bulletDirection.magnitude))
        {
            Collider objectCollider = raycastHit.transform.GetComponent<Collider>();
            if(objectCollider != null)
            {
                Debug.Log("Hit this collider" + objectCollider);
                SoldierBehaviour soldier = objectCollider.transform.GetComponent<SoldierBehaviour>();
                Debug.Log(soldier);
                if (soldier!= null)
                {
                    soldier.Die();
                }
            }


            
            BulletDespawn();
        }

        _lastBulletLocation = transform.position;
        _startedMoving = true;
    }

    
    public void BulletDespawn()
    {
        Destroy(gameObject);
    }
}
