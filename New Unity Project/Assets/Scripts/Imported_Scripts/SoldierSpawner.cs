﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierSpawner : MonoBehaviour
{
    public Transform[] _spawnPoint;
    public GameObject _enemyUnit;
    public float _spawnDelay = 1f;
    private float _spawnTimer;
    private bool _startedSpawning;


    private void Awake()
    {

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SpawnEnemyUnit();
            _startedSpawning = true;
        }
        _spawnTimer -= Time.deltaTime;

        if (_spawnTimer <= 0 && _startedSpawning)
        {
            SpawnEnemyUnit();
        }
    }

    public void SpawnEnemyUnit()
    {
        _startedSpawning = true;
        Transform spawnTransform = _spawnPoint[Random.Range(0, _spawnPoint.Length)];
        Instantiate(_enemyUnit, spawnTransform);
        _spawnTimer = _spawnDelay;
    }
}
