﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierBehaviour : MonoBehaviour
{
    private Rigidbody[] _rigidbodies;
    private float _movementSpeedMultiplier = 2f;


    private bool _isAlive = true;



    private void Awake()
    {
        _isAlive = true;
       _rigidbodies = GetComponentsInChildren<Rigidbody>();
        foreach(Rigidbody rigidbody in _rigidbodies)
        {
            rigidbody.isKinematic = true;

        }
    }



    private void Update()
    {
    //Transform.translate to move the enemy forward
        if(_isAlive != false)
        {
        transform.Translate(Vector3.forward * Time.deltaTime * _movementSpeedMultiplier);

        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            Die();
        }
    }
    [ContextMenu("Kill")]
    public void Die()
    {
        GetComponent<Animator>().runtimeAnimatorController = null;
        GetComponent<Collider>().enabled = false;
        Debug.Log(GetComponent<Animator>().runtimeAnimatorController);
        foreach (Rigidbody rigidbody in _rigidbodies)
        {
            Debug.Log(rigidbody);
            rigidbody.isKinematic = false;
            _isAlive = false;
            
        }

    }
}
