﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targets : MonoBehaviour
{
    //public GameObject _target;
    public EventManager _eventManager;

    private void OnCollisionEnter(Collision collision)
    {
        _eventManager.AddPoints();
        Debug.Log("got hit");
        Destroy(gameObject);
    }
}
