﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBehaviour : MonoBehaviour
{
    public Transform _barrelEnd;
    public GameObject _bulletToSpawn;
    public float _bulletSpeedMultiplier = 500;
    private GameObject _spawnedBullet;

    public AudioSource _gunFire;


    public MagazineSlot _magazineSlot;

    public void FireGun()
    {

        HouseSpawner house = _magazineSlot._currentMagazine;
            if (_magazineSlot != null && house._roundsLeft > 0  && house != null)
            {
                _spawnedBullet = Instantiate(_bulletToSpawn, _barrelEnd.position, _barrelEnd.rotation);
                Rigidbody bulletRigidbody = _spawnedBullet.GetComponent<Rigidbody>();
                bulletRigidbody.AddForce(transform.forward * _bulletSpeedMultiplier * Time.deltaTime, ForceMode.Impulse);
                house._roundsLeft--;
                _gunFire.Play();
            }
        

    }

}
