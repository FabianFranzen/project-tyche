﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class HandBehaviour : MonoBehaviour
{
    public SteamVR_Input_Sources _hand;

    public Rigidbody _heldObject = null;

    private static bool _isHoldingGun = false;
    public GunBehaviour _gunBehaviour;

    public SoldierSpawner _soldierSpawner;

    private bool _isSpawningEnemies;

    private void Awake()
    {
        _gunBehaviour.GetComponent<GunBehaviour>();
    }

    protected void Update()
    {
        if (SteamVR_Actions.default_GrabGrip.GetStateDown(_hand))
        {
            GrabbedObject();
        }

        if (SteamVR_Actions.default_GrabGrip.GetStateUp(_hand))
        {
            DroppedObject();
        }

        if (SteamVR_Actions.default_GrabPinch.GetStateDown(_hand) && _isHoldingGun == true)
        {
            Debug.Log("Fire");
            _gunBehaviour.FireGun(); 
        }

        if (Input.GetKeyDown(KeyCode.Space) && !_isSpawningEnemies)
        {

        }

        if (SteamVR_Actions.default_Teleport.GetStateDown(_hand) && !_isSpawningEnemies)
        {
            _soldierSpawner.SpawnEnemyUnit();
            _isSpawningEnemies = true;
        }
     
    }



    public void GrabbedObject()
    {
        Collider[] overlappingObjects = Physics.OverlapSphere(transform.position, 0.05f);
        
        foreach  (Collider collider in overlappingObjects)
        {
            
            if (collider.GetComponent<InteractableObject>())
            {
                Rigidbody rigidbody = collider.GetComponent<Rigidbody>();
                if (rigidbody != null)
                {
                    StartHoldingObject(rigidbody);
                    GunBehaviour rifle = _heldObject.GetComponent<GunBehaviour>();
                    if(rifle)
                    {
                        _isHoldingGun = true;

                    }


                    HouseSpawner magazine = _heldObject.GetComponent<HouseSpawner>();
                    if(magazine != null)
                    {
                        magazine.AssignHolder(this);
                            Debug.Log(magazine);
                        // is this magazine attached to a rifle? if so, detach it
                        if(magazine._magazineSlot != null && _isHoldingGun)
                        {
                            magazine._magazineSlot.DetachMagazine();
                        }
                        
                    }
                }
                //Get bucket Component and give player a magazine

                AmmoBucket ammoBucket = collider.GetComponent < AmmoBucket>();
                if(ammoBucket)
                {
                    HouseSpawner magazine = ammoBucket.GetNewBuilding();
                    magazine.transform.position = transform.position;
                    magazine.transform.rotation = transform.rotation;
                    magazine.AssignHolder(this);
                    StartHoldingObject(magazine.GetComponent<Rigidbody>());
                }
                break;
            }
        }
    }

    private void StartHoldingObject(Rigidbody rigidbody)
    {
        rigidbody.transform.SetParent(transform);
        rigidbody.isKinematic = true;
        _heldObject = rigidbody;

    }

    public void DroppedObject()
    {
        if(_heldObject != null)
        {

            _heldObject.transform.SetParent(null);
            _heldObject.isKinematic = false;

          /*  GunBehaviour rifle = _heldObject.GetComponent<GunBehaviour>();
            if (rifle)
            {
                _isHoldingGun = false;
            }*/

            _heldObject = null;
        }
    }
}
