﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBucket : MonoBehaviour
{
    public HouseSpawner _buildingPrefab;
    //Instantiate magazine
    public HouseSpawner GetNewBuilding()
    {
        HouseSpawner spawnedBuilding = Instantiate(_buildingPrefab, transform.position, transform.rotation);

        return spawnedBuilding;
    }
}
